package studentlist;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
    
    private String name;

    public Student(String name) {
        this.name = name;
    }

    
    public void setName(String name) {
        this.name = name;
    }
    
    public String program;
    
    public Student(String program){
    this.program=program;
    
    }
       public String getProgram() {
        return program;
    }
    
       public void setProgram(String name) {
        this.program = program;
    }

}

